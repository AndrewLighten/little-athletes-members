package au.com.actapps.members.logic;

import au.com.actapps.members.logic.impl.AgeGroupCalculatorImpl;

import org.joda.time.DateTime;

/**
 * The {@code AgeGroupCalculatorFactory} class knows how to build an age group calculator.
 *
 * @author andrew.lighten@gmail.com
 */
public class AgeGroupCalculatorFactory {

  /**
   * Get the default age group calculator implementation.
   *
   * @return The age group calculator implementation.
   */
  public AgeGroupCalculator getAgeGroupCalculator() {
    return new AgeGroupCalculatorImpl(DateTime.now().withTimeAtStartOfDay());
  }

} // factory
