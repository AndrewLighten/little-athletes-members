package au.com.actapps.members.logic.impl;

import au.com.actapps.members.logic.AgeGroupCalculator;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The {@code AgeGroupCalculatorImpl} class provides a default implementation for the age group calculator. It assumes that the age
 * group ranges are based on date of birth as-of 1st October, as per Little Athletics Victoria standards.
 *
 * @author andrew.lighten@gmail.com
 */
public class AgeGroupCalculatorImpl implements AgeGroupCalculator {

  /**
   * Application logfile.
   */
  private final Logger LOG = LoggerFactory.getLogger(AgeGroupCalculatorImpl.class);

  /**
   * Age group list.
   */
  private final List<AgeGroup> ageGroupList = new ArrayList<>();

  /**
   * Season start date.
   */
  private static final int SEASON_START_DAY = 1;
  private static final int SEASON_START_MONTH = 10;

  /**
   * Constructs a new age group calculator.
   *
   * @param calculationDate The date that calculations should be based on. In most situations, this will be today's date, but for
   *                        testing, having a fixed date is useful.
   */
  public AgeGroupCalculatorImpl(final DateTime calculationDate) {

    // preconditions.
    checkNotNull(calculationDate);

    // find the closest October 1st.
    final DateTime nearestSeasonStart = findNearestSeasonStart(calculationDate);

    // setup the age groups — U6 to U16
    for (int group = 6; group <= 16; group++) {
      final DateTime startDate = nearestSeasonStart.withYear(nearestSeasonStart.getYear() - group);
      final DateTime endDate = nearestSeasonStart.withYear(nearestSeasonStart.getYear() - group + 1).minusDays(1);
      final AgeGroup ageGroup = new AgeGroup(group, startDate, endDate);
      ageGroupList.add(ageGroup);
    }
  }

  /**
   * Given an athlete's date of birth, get a numeric representation of the age group they are in. An athlete whose date of birth
   * causes them to be in under 9s, for example, will result in 9 being returned.
   *
   * @param dateOfBirth The athlete's date of birth.
   * @return The athlete's age group, if they fall within the valid age group ranges; otherwise, null.
   */
  @Override
  public Integer getAgeGroup(final DateTime dateOfBirth) {

    // preconditions.
    checkNotNull(dateOfBirth);

    // find the age group that the date of birth matches.
    for (final AgeGroup ageGroup : ageGroupList) {
      if (dateOfBirth.isEqual(ageGroup.startDate) || dateOfBirth.isEqual(ageGroup.endDate))
        return ageGroup.ageGroup;
      if (dateOfBirth.isAfter(ageGroup.startDate) && dateOfBirth.isBefore(ageGroup.endDate))
        return ageGroup.ageGroup;
    }

    // no data yet.
    return null;
  }

  /**
   * Find the nearest season start date.
   *
   * @param calculationDate The calculation date.
   * @return The date of the nearest season start.
   */
  private DateTime findNearestSeasonStart(final DateTime calculationDate) {

    // preconditions.
    checkNotNull(calculationDate);

    // let's create three dates -- the season start on the year before the calculation date, the year of the calculation date,
    // and the year after the calculation date.
    final DateTime lastYear = new DateTime().withDayOfMonth(SEASON_START_DAY).withMonthOfYear(SEASON_START_MONTH).withYear(calculationDate.getYear() - 1).withTimeAtStartOfDay();
    final DateTime thisYear = new DateTime().withDayOfMonth(SEASON_START_DAY).withMonthOfYear(SEASON_START_MONTH).withYear(calculationDate.getYear()).withTimeAtStartOfDay();
    final DateTime nextYear = new DateTime().withDayOfMonth(SEASON_START_DAY).withMonthOfYear(SEASON_START_MONTH).withYear(calculationDate.getYear() + 1).withTimeAtStartOfDay();

    // see how far each one is from the calculation date.
    int lastYearDelta = Math.abs(Days.daysBetween(calculationDate, lastYear).getDays());
    int thisYearDelta = Math.abs(Days.daysBetween(calculationDate, thisYear).getDays());
    int nextYearDelta = Math.abs(Days.daysBetween(calculationDate, nextYear).getDays());

    // pick the closest one.
    int closest = Math.min(lastYearDelta, Math.min(thisYearDelta, nextYearDelta));
    if (lastYearDelta == closest)
      return lastYear;
    else if (thisYearDelta == closest)
      return thisYear;
    else
      return nextYear;
  }

  /**
   * This age group class represents an age group and the matching years.
   */
  private static class AgeGroup {

    final int ageGroup;
    final DateTime startDate;
    final DateTime endDate;

    public AgeGroup(final int ageGroup, final DateTime startDate, final DateTime endDate) {
      this.ageGroup = ageGroup;
      this.startDate = checkNotNull(startDate);
      this.endDate = checkNotNull(endDate);
    }

  } // age group

} // age group calculator
