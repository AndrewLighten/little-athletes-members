package au.com.actapps.members.logic;

import org.joda.time.DateTime;

/**
 * The {@code AgeGroupCalculator} interface describes a class that can translate a date-of-birth into an age group. It uses the
 * current date and time to work out what the age group cutoff dates are, and based on that, can determine which age group an
 * athlete falls into.
 *
 * @author andrew.lighten@gmail.com
 */
public interface AgeGroupCalculator {

  /**
   * Given an athlete's date of birth, get a numeric representation of the age group they are in. An athlete whose date of birth
   * causes them to be in under 9s, for example, will result in 9 being returned.
   *
   * @param dateOfBirth The athlete's date of birth.
   * @return The athlete's age group, if they fall within the valid age group ranges; otherwise, null.
   */
  Integer getAgeGroup(final DateTime dateOfBirth);

} // class
