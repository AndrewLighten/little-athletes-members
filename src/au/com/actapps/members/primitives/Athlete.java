package au.com.actapps.members.primitives;

import org.joda.time.DateTime;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The {@code Athlete} class represents a registered athlete.
 *
 * @author andrew.lighten@gmail.com
 */
public class Athlete implements Comparable<Athlete> {

  /**
   * The athlete's name.
   */
  private final String name;

  /**
   * The athlete's surname.
   */
  private final String surname;

  /**
   * The athlete's date of birth.
   */
  private final DateTime dob;

  /**
   * The athlete's gender.
   */
  private final Gender gender;

  /**
   * The athlete's age group.
   */
  private final Integer ageGroup;

  /**
   * Get the athlete's name.
   */
  public String getName() {
    return name;
  }

  /**
   * Get the athlete's surname.
   */
  public String getSurname() {
    return surname;
  }

  /**
   * Get the athlete's date of birth.
   */
  public DateTime getDob() {
    return dob;
  }

  /**
   * Get the athlete's gender.
   */
  public Gender getGender() {
    return gender;
  }

  /**
   * Get the athlete's age group.
   */
  public Integer getAgeGroup() {
    return ageGroup;
  }

  /**
   * Get a description of the athlete's age group.
   */
  public String getAgeGroupDescription() {

    return (ageGroup != null) ? "U" + ageGroup + gender.getGenderIdentifier() : null;
  }

  /**
   * Construct a new athlete from an athlete builder.
   *
   * @param builder The builder.
   */
  private Athlete(final Builder builder) {

    // preconditions.
    checkNotNull(builder);

    // populate our attributes.
    name = builder.name;
    surname = builder.surname;
    dob = builder.dob;
    gender = builder.gender;
    ageGroup = builder.ageGroup;
  }

  /**
   * Compares this object with the specified object for order.
   *
   * @param that the object to be compared.
   * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified
   * object.
   */
  @Override
  public int compareTo(final Athlete that) {
    return this.getName().compareTo(that.getName());
  }

  /**
   * This class is used to build athletes.
   */
  public static class Builder {

    private String name;
    private String surname;
    private DateTime dob;
    private Gender gender;
    private Integer ageGroup;

    /**
     * Use this builder to create a new athlete.
     *
     * @return The new athlete.
     */
    public Athlete build() {
      return new Athlete(this);
    }

    /**
     * Set the athlete's name.
     *
     * @param name The athlete's name.
     * @return This builder.
     */
    public Builder setName(final String name) {
      this.name = checkNotNull(name);
      return this;
    }

    /**
     * Set the athlete's surname.
     *
     * @param surname The athlete's surname.
     * @return This builder.
     */
    public Builder setSurname(final String surname) {
      this.surname = checkNotNull(surname);
      return this;
    }

    /**
     * Set the athlete's date of birth.
     *
     * @param dob The athlete's date of birth.
     * @return This builder.
     */
    public Builder setDob(final DateTime dob) {
      this.dob = checkNotNull(dob);
      return this;
    }

    /**
     * Set the athlete's gender.
     *
     * @param gender The athlete's gender.
     * @return This builder.
     */
    public Builder setGender(final Gender gender) {
      this.gender = checkNotNull(gender);
      return this;
    }

    /**
     * Set the athlete's age group.
     *
     * @param ageGroup The athlete's age group.
     * @return This builder.
     */
    public Builder setAgeGroup(final Integer ageGroup) {
      this.ageGroup = ageGroup;
      return this;
    }

  } // builder

} // athlete
