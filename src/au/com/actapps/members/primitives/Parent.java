package au.com.actapps.members.primitives;

import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The {@code Parent} class represents a Little Athletics parent who has one or more children signed up as athletes.
 *
 * @author andrew.lighten@gmail.com
 */
public class Parent implements Comparable<Parent> {

  /**
   * The parent's name.
   */
  private final String parentName;

  /**
   * The parent's contact number.
   */
  private final String contactNumber;

  /**
   * The parent's email address.
   */
  private final String email;

  /**
   * The parent's address.
   */
  private final String address;

  /**
   * The parent's suburb.
   */
  private final String suburb;

  /**
   * The parent's sort key.
   */
  private final String sortKey;

  /**
   * The list of athletes associated with this parent.
   */
  public List<Athlete> athleteList;

  /**
   * Get the parent's name.
   */
  public String getParentName() {
    return parentName;
  }

  /**
   * Get the parent's contact number.
   */
  public String getContactNumber() {
    return contactNumber;
  }

  /**
   * Get the parent's email address.
   */
  public String getEmail() {
    return email;
  }

  /**
   * Get the parent's address.
   */
  public String getAddress() {
    return address;
  }

  /**
   * Get the parent's suburb.
   */
  public String getSuburb() {
    return suburb;
  }

  /**
   * Get the list of athletes associated with this parent.
   */
  public ImmutableList<Athlete> getAthleteList() {
    Collections.sort(athleteList);
    return ImmutableList.copyOf(athleteList);
  }

  /**
   * Associate an athlete with this parent.
   *
   * @param athlete The athlete.
   */
  public void addAthlete(final Athlete athlete) {
    athleteList.add(checkNotNull(athlete));
  }

  /**
   * Construct a new parent from a parent builder.
   *
   * @param builder The builder.
   */
  private Parent(final Builder builder) {

    // preconditions.
    checkNotNull(builder);

    // populate our attributes.
    parentName = builder.parentName;
    contactNumber = builder.contactNumber;
    email = builder.email;
    address = builder.address;
    suburb = builder.suburb;
    sortKey = builder.sortKey;
    athleteList = new ArrayList<>();
  }

  /**
   * Compares this object with the specified object for order.
   *
   * @param that the object to be compared.
   * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified
   * object.
   */
  @Override
  public int compareTo(final Parent that) {
    return this.sortKey.compareTo(that.sortKey);
  }

  /**
   * This class is used to build parents.
   */
  public static class Builder {

    private String parentName;
    private String contactNumber;
    private String email;
    private String address;
    private String suburb;
    private String sortKey;

    /**
     * Use this builder to create a new parent.
     *
     * @return The new parent.
     */
    public Parent build() {
      return new Parent(this);
    }

    /**
     * Set the parent's name.
     *
     * @param parentName The parent's name.
     * @return This builder.
     */
    public Builder setParentName(final String parentName) {
      this.parentName = checkNotNull(parentName);
      return this;
    }

    /**
     * Set the parent's contact number.
     *
     * @param contactNumber The parent's contact number.
     * @return This builder.
     */
    public Builder setContactNumber(final String contactNumber) {
      this.contactNumber = contactNumber;
      return this;
    }

    /**
     * Set the parent's email address.
     *
     * @param email The parent's email address.
     * @return This builder.
     */
    public Builder setEmail(final String email) {
      this.email = email;
      return this;
    }

    /**
     * Set the parent's address.
     *
     * @param address The parent's address.
     * @return This builder.
     */
    public Builder setAddress(final String address) {
      this.address = address;
      return this;
    }

    /**
     * Set the parent's suburb.
     *
     * @param suburb The parent's suburb.
     * @return This builder.
     */
    public Builder setSuburb(final String suburb) {
      this.suburb = suburb;
      return this;
    }

    /**
     * Set the parent's sort key.
     *
     * @param sortKey The parent's sort key.
     * @return This builder.
     */
    public Builder setSortKey(final String sortKey) {
      this.sortKey = sortKey;
      return this;
    }

  } // builder

} // parent
