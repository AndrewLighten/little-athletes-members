package au.com.actapps.members.primitives;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The {@code Gender} enumeration represents the registered gender of an athlete.
 *
 * @author andrew.lighten@gmail.com
 */
public enum Gender {

  /**
   * Male athlete.
   */
  Male("M"),

  /**
   * Female athlete.
   */
  Female("F");

  /**
   * The internal gender identifier.
   */
  private final String genderIdentifier;

  /**
   * Constructs a gender enumeration.
   *
   * @param genderIdentifier The gender identifier.
   */
  private Gender(final String genderIdentifier) {
    this.genderIdentifier = checkNotNull(genderIdentifier);
  }

  /**
   * Get the internal gender identifier.
   *
   * @return The gender identifier.
   */
  public final String getGenderIdentifier() {
    return this.genderIdentifier;
  }

} // gender
