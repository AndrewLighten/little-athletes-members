package au.com.actapps.members.parsing;

import au.com.actapps.members.parsing.impl.ParserImpl;

/**
 * The {@code ParserFactory} class knows how to build a parser.
 *
 * @author andrew.lighten@gmail.com
 */
public class ParserFactory {

  /**
   * Get the default parser implementation.
   *
   * @return The parser implementation.
   */
  public Parser getParser() {
    return new ParserImpl();
  }

} // parser factory
