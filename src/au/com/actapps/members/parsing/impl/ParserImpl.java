package au.com.actapps.members.parsing.impl;

import com.google.common.base.CaseFormat;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import au.com.actapps.members.logic.AgeGroupCalculator;
import au.com.actapps.members.logic.AgeGroupCalculatorFactory;
import au.com.actapps.members.parsing.Parser;
import au.com.actapps.members.primitives.Athlete;
import au.com.actapps.members.primitives.Gender;
import au.com.actapps.members.primitives.Parent;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The {@code ParserImpl} class provides a default implementation of the parsing interface. It should not be instantiated directly
 * -- instead, go via the {@code ParserFactory} class.
 *
 * @author andrew.lighten@gmail.com
 */
public class ParserImpl implements Parser {

  /**
   * Application logfile.
   */
  private final Logger LOG = LoggerFactory.getLogger(ParserImpl.class);

  /**
   * The date/time pattern we use.
   */
  private static final String PATTERN = "d-MMM-yy";

  /**
   * The date/time formatter.
   */
  private static final DateTimeFormatter dtp = DateTimeFormat.forPattern(PATTERN);

  /**
   * The CSV file fields we use.
   */
  private static final String ATHLETE_NAME = "Name";
  private static final String ATHLETE_SURNAME = "Surname";
  private static final String ATHLETE_DATE_OF_BIRTH = "D.O.B";
  private static final String ATHLETE_GENDER = "Gender";
  private static final String PARENT_NAME = "Parent1name";
  private static final String PARENT_SURNAME = "Parent1surname";
  private static final String PARENT_CONTACT_NUMBER = "Primarycontactnumber";
  private static final String PARENT_EMAIL = "Email";
  private static final String PARENT_ADDRESS = "Address";
  private static final String PARENT_SUBURB = "Suburb";

  /**
   * Construct a new parser implementation.
   */
  public ParserImpl() {

  }

  /**
   * Parse the member .csv file and fetch the list of parents. Each parent will have their associated athletes attached.
   *
   * @param csvfile The path of the .csv file to parse and load from.
   * @return The list of parents.
   */
  @Override
  public ImmutableList<Parent> getParentsFromCsvFile(final String csvfile) {

    // preconditions.
    checkNotNull(csvfile);

    // load the file into a set of basic {@code CSVRecord} objects.
    final ImmutableList<CSVRecord> csvRecords = loadCsvRecords(csvfile);
    if (csvRecords == null)
      return null;

    // turn each CSV record into a parent with its associated athletes.
    return parseFamilyStructure(csvRecords);
  }

  /**
   * Load the collection of CSV records found in a file.
   *
   * @param csvFile The CSV file.
   * @return The list of CSV records.
   */
  private ImmutableList<CSVRecord> loadCsvRecords(final String csvFile) {

    // preconditions.
    checkNotNull(csvFile);

    // load the CSV records.
    try (final Reader in = new FileReader(csvFile)) {
      final Iterable<CSVRecord> csvRecords = CSVFormat.EXCEL.withHeader().parse(in);
      return ImmutableList.copyOf(csvRecords);
    }

    // something went very, very wrong.
    catch (final Exception e) {
      LOG.error("Failed to load CSV file: {}", e.getMessage());
      return null;
    }
  }

  /**
   * Go over the raw CSV records loaded from disk, and turn that into a structure of parents with attached athletes.
   *
   * @param csvRecords The CSV records.
   * @return The list of parents.
   */
  private ImmutableList<Parent> parseFamilyStructure(final ImmutableList<CSVRecord> csvRecords) {

    // preconditions.
    checkNotNull(csvRecords);

    // construct our loading context.
    final LoadingContext ctx = new LoadingContext();

    // visit each CSV record.
    for (final CSVRecord csvRecord : csvRecords) {

      // see if this parent exists -- if not, create it
      final Parent parent = ctx.findOrCreateParent(csvRecord);
      if (parent == null)
        continue;

      // find the athlete from this line.
      final Athlete athlete = ctx.createAthlete(csvRecord);
      if (athlete != null && athlete.getAgeGroupDescription() != null)
        parent.addAthlete(athlete);
    }

    // done.
    return ctx.getParentList();
  }

  /**
   * This class represents the context of a load operation.
   */
  private class LoadingContext {

    /**
     * The dictionary of parents.
     */
    final Dictionary<String, Parent> parentDictionary;

    /**
     * The age group calculator.
     */
    final AgeGroupCalculator ageGroupCalculator;

    // setup the ag

    /**
     * Construct a new loading context.
     */
    private LoadingContext() {

      parentDictionary = new Hashtable<>();
      ageGroupCalculator = new AgeGroupCalculatorFactory().getAgeGroupCalculator();
    }

    /**
     * Find a parent, creating it if it's not there.
     *
     * @param csvRecord The CSV record holding the parent's details.
     * @return The {@code Parent} that this CSV record represents, or {@code null} if the parent can't be created.
     */
    private Parent findOrCreateParent(final CSVRecord csvRecord) {

      // preconditions.
      checkNotNull(csvRecord);

      // find the parent's name. if we can't identify the name, then we fail.
      final String parentName = getParentName(csvRecord);
      if (Strings.isNullOrEmpty(parentName)) {
        LOG.warn("Ignoring record {} -- no \"{}\" field", csvRecord.getRecordNumber(), PARENT_NAME);
        return null;
      }

      // phone numbers are all over the place -- let's fixed that.
      final String fixedPhone = fixPhoneNumber(csvRecord.get(PARENT_CONTACT_NUMBER));

      // see if it's in the dictionary.
      final Parent existingParent = parentDictionary.get(parentName);
      if (existingParent != null)
        return existingParent;

      // create the parent via the parent builder.
      final Parent parent = new Parent.Builder().setParentName(parentName)
                                                .setContactNumber(fixedPhone)
                                                .setEmail(csvRecord.get(PARENT_EMAIL))
                                                .setAddress(csvRecord.get(PARENT_ADDRESS))
                                                .setSuburb(csvRecord.get(PARENT_SUBURB))
                                                .setSortKey(parentName.toLowerCase())
                                                .build();

      // add this parent to the dictionary.
      parentDictionary.put(parentName, parent);

      // done.
      return parent;
    }

    /**
     * Get the parent's name.
     *
     * @param csvRecord The CSV record to take the name from.
     * @return The parent's name.
     */
    private String getParentName(final CSVRecord csvRecord) {

      // preconditions.
      checkNotNull(csvRecord);

      // find the first name, split into words, then fix each word.
      final String name = csvRecord.get(PARENT_NAME);
      final Iterable<String> rawNameWords = Splitter.on(' ').split(name);
      final List<String> nameWords = splitAndFixNameWords(rawNameWords);

      // do the same with the surname.
      final String surname = csvRecord.get(PARENT_SURNAME);
      final Iterable<String> rawSurnameWords = Splitter.on(' ').split(surname);
      final List<String> surnameWords = splitAndFixNameWords(rawSurnameWords);

      // for the surname, there are three patterns that we can see:
      //
      // (1) the name doesn't include the surname -- i.e., "fred","smith"
      // (2) the name includes the surname, which is then duplicate -- i.e., "fred smith","smith"
      // (3) the name includes a surname and a different family's surname -- i.e., "fred smith","bloggs"
      //
      // depending on what we can grok, we'll take the following actions:
      //
      // (1) concatenate the two values, yielding "fred smith"
      // (2) ignore the surname, yielding "fred smith"
      // (3) concatenate the two values with the surname in parentheses, yielding "fred smith (bloggs)"

      // check for scenario 1
      if (nameWords.size() == 1 && surnameWords.size() > 0)
        nameWords.addAll(surnameWords);

        // check for scenario 2
      else if (nameWords.size() >= surnameWords.size() && surnameWords.size() > 0 && nameWords.subList(nameWords.size() - surnameWords.size(), nameWords.size()).equals(surnameWords))
        return Joiner.on(" ").join(nameWords);

        // check for scenario 3
      else if (nameWords.size() > 0 && surnameWords.size() > 0)
        nameWords.add("(" + Joiner.on(" ").join(surnameWords) + ")");

      // combine the name words with spaces
      return Joiner.on(" ").join(nameWords);
    }

    /**
     * Take a list of words that form part of a name, and normalise them. This normalisation will correct them to uppercase, convert
     * "And" to "&", and a few other tweaks.
     *
     * @param nameWords The list of name words.
     * @return The corrected list of name words.
     */
    private List<String> splitAndFixNameWords(final Iterable<String> nameWords) {

      // preconditions.
      checkNotNull(nameWords);

      // setup the list of fixed words.
      final List<String> wordList = new ArrayList<>();

      // visit each word.
      for (final String word : nameWords) {

        // turn the whole word into correct case -- leading uppercase the remainder lowercase.
        String fixedWord = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, word);

        // fix words with a missing apostrophe. this is risky...
        if (fixedWord.startsWith("Obr"))
          fixedWord = "O'Br" + fixedWord.substring(3);

        // fix instances of (1) "Mcdonald", and (2) "O'brien", "O'connor", etc...
        if (fixedWord.startsWith("Mc") && fixedWord.length() > 2)
          fixedWord = "Mc" + CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, fixedWord.substring(2));
        else if (fixedWord.startsWith("O'") && fixedWord.length() > 2)
          fixedWord = "O'" + CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, fixedWord.substring(2));

        // fix hyphenated names
        fixedWord = fixedWord.replaceAll("/", "-");
        if (fixedWord.contains("-")) {
          final Iterable<String> rawWordList = Splitter.on("-").split(fixedWord);
          final List<String> fixedWordList = splitAndFixNameWords(rawWordList);
          fixedWord = Joiner.on("-").join(fixedWordList);
        }

        // replace "And" and "Or" with an ampersand.
        if (fixedWord.equals("And") || fixedWord.equals("Or"))
          fixedWord = "&";

        // store the word.
        wordList.add(fixedWord);
      }

      // done.
      return wordList;
    }

    /**
     * Fix a phone number. We're expecting all numbers to be Australian, so we'll strip any leading international codes
     * (specifically, "61"), then any leading zeroes, then we'll pad to 10 digits with correct spacing.
     *
     * @param rawPhoneNumber The raw phone number.
     * @return The fixed phone number.
     */
    private String fixPhoneNumber(final String rawPhoneNumber) {

      // if there's no phone number, no change.
      if (Strings.isNullOrEmpty(rawPhoneNumber))
        return rawPhoneNumber;

      // extract all the digits - ignore spaces, hyphens, dots, etc.
      String digits = rawPhoneNumber.replaceAll("\\D+", "");

      // if it starts with an internal code, drop that
      if (digits.startsWith("61"))
        digits = digits.substring(2);

      // we expect at least 8 digits
      if (digits.length() < 9)
        return rawPhoneNumber;

      // pad with leading zero if required
      if (digits.length() == 9)
        digits = "0" + digits;

      // ok, now we'll format using Australian convention
      assert digits.length() == 10;
      if (digits.startsWith("04"))
        return digits.substring(0, 4) + " " + digits.substring(4, 7) + " " + digits.substring(7); // 04xx xxx xxx
      else
        return digits.substring(0, 2) + " " + digits.substring(2, 6) + " " + digits.substring(6); // 02 xxxx xxxx, 03 xxxx xxxx, etc...
    }

    /**
     * Create the athlete.
     *
     * @param csvRecord The CSV record.
     * @return The created athlete.
     */
    private Athlete createAthlete(final CSVRecord csvRecord) {

      // preconditions.
      checkNotNull(csvRecord);

      // find and massage the athlete's name
      final String name = csvRecord.get(ATHLETE_NAME);
      final Iterable<String> rawNameWords = Splitter.on(' ').split(name);
      final List<String> nameWords = splitAndFixNameWords(rawNameWords);

      // do the same with the surname.
      final String surname = csvRecord.get(ATHLETE_SURNAME);
      final Iterable<String> rawSurnameWords = Splitter.on(' ').split(surname);
      final List<String> surnameWords = splitAndFixNameWords(rawSurnameWords);

      // parse the date of birth.
      final DateTime dateOfBirth = dtp.parseDateTime(csvRecord.get(ATHLETE_DATE_OF_BIRTH));

      // determine the age group.
      final Integer ageGroup = ageGroupCalculator.getAgeGroup(dateOfBirth);
      if (ageGroup == null)
        LOG.warn("Date of birth {} is outside age group range", ageGroup);

      // find the athlete details.
      final Athlete.Builder builder = new Athlete.Builder();
      builder.setName(Joiner.on(" ").join(nameWords))
             .setSurname(Joiner.on(" ").join(surnameWords))
             .setDob(dateOfBirth)
             .setGender(csvRecord.get(ATHLETE_GENDER).equalsIgnoreCase(Gender.Male.getGenderIdentifier()) ? Gender.Male : Gender.Female)
             .setAgeGroup(ageGroupCalculator.getAgeGroup(dateOfBirth));

      // build the athlete.
      return builder.build();
    }

    /**
     * Get the list of parents.
     */
    private ImmutableList<Parent> getParentList() {

      // fetch the parent list, then sort it
      final List<Parent> parentList = Collections.list(parentDictionary.elements());
      Collections.sort(parentList);

      // transform the parent dictionary to a list, then return an immutable representation of that list.
      return ImmutableList.copyOf(parentList);
    }

  } // loading context

} // parser implementation
