package au.com.actapps.members.parsing;

import com.google.common.collect.ImmutableList;

import au.com.actapps.members.primitives.Parent;

/**
 * The {@code Parser} interface describes the capabilities of a member file parser.
 *
 * @author andrew.lighten@gmail.com
 */
public interface Parser {

  /**
   * Parse the member .csv file and fetch the list of parents. Each parent will have their associated athletes attached.
   *
   * @param csvfile The path of the .csv file to parse and load from.
   * @return The list of parents.
   */
  public ImmutableList<Parent> getParentsFromCsvFile(final String csvfile);

} // parser
