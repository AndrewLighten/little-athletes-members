
package au.com.actapps.members;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import au.com.actapps.members.parsing.Parser;
import au.com.actapps.members.parsing.ParserFactory;
import au.com.actapps.members.primitives.Athlete;
import au.com.actapps.members.primitives.Parent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

  private final static Logger LOG = LoggerFactory.getLogger(Main.class);

  public static void main(String[] args) {

    final Parser parser = new ParserFactory().getParser();

    final ImmutableList<Parent> parents = parser.getParentsFromCsvFile("/Users/andrew/Downloads/MemberReport.csv");
    for (final Parent parent : parents) {

      if (parent.getAthleteList().isEmpty())
        continue;

      final StringBuilder sb = new StringBuilder();
      for (final Athlete athlete : parent.getAthleteList()) {
        if (sb.length() > 0)
          sb.append(", ");
        sb.append(athlete.getName()).append(" ").append(athlete.getSurname()).append(" (").append(athlete.getAgeGroupDescription()).append(")");
      }

      System.out.println("");
      System.out.println(parent.getParentName());
      if (!Strings.isNullOrEmpty(parent.getContactNumber()))
        System.out.println("  p| " + parent.getContactNumber());
      if (!Strings.isNullOrEmpty(parent.getEmail()))
        System.out.println("  e| " + parent.getEmail().toLowerCase());
      System.out.println("   | " + sb.toString());
    }
  }

} // main

