# README #

This application provides a general framework for manipulation of a Little Athletes member list. To use it:

1. Go to centreadmin.resultshq.com.au
2. Log in with your centre ID.
3. View the member list.
4. Export the member list to Excel.
5. Open the member list in Excel, then save as a .csv file.
6. Run this application against the member .csv file.

Note that right now, it does almost nothing -- but it will be built out fairly quickly.

### What is this repository for? ###

* Allows for offline (and read-only) manipulation of a Little Athletics member list.
* Version 0.1

### How do I get set up? ###

This is an IntelliJ IDEA project. If you're using Eclipse, you're on your own.

### Who do I talk to? ###

* Created by andrew.lighten@gmail.com (Maryborough Little Athletics Centre, Victoria).
* Ping me if you need help.