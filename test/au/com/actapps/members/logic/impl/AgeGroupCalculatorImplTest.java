package au.com.actapps.members.logic.impl;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Assert;
import org.junit.Test;

/**
 * The {@code AgeGroupCalculatorImplTest} class is able to test the age group calculator.
 *
 * @author andrew.lighten@gmail.com
 */
public class AgeGroupCalculatorImplTest {

  /**
   * The date/time pattern we use.
   */
  private static final String PATTERN = "dd-MMM-yyyy";

  /**
   * The date/time formatter.
   */
  private static final DateTimeFormatter parser = DateTimeFormat.forPattern(PATTERN);

  /**
   * Test the age group calculator.
   *
   * @throws Exception if something terrible happens.
   */
  @Test
  public void testU6ForOct2014() throws Exception {

    // setup a calculator for the 2014/2015 season
    final AgeGroupCalculatorImpl ageGroupCalculator = new AgeGroupCalculatorImpl(parser.parseDateTime("1-Oct-2014"));

    // check U6
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("1-Oct-2008")), new Integer(6));
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("30-Sep-2009")), new Integer(6));
  }

  /**
   * Test the age group calculator.
   *
   * @throws Exception if something terrible happens.
   */
  @Test
  public void testU7ForOct2014() throws Exception {

    // setup a calculator for the 2014/2015 season
    final AgeGroupCalculatorImpl ageGroupCalculator = new AgeGroupCalculatorImpl(parser.parseDateTime("1-Oct-2014"));

    // check U7
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("1-Oct-2007")), new Integer(7));
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("30-Sep-2008")), new Integer(7));
  }

  /**
   * Test the age group calculator.
   *
   * @throws Exception if something terrible happens.
   */
  @Test
  public void testU8ForOct2014() throws Exception {

    // setup a calculator for the 2014/2015 season
    final AgeGroupCalculatorImpl ageGroupCalculator = new AgeGroupCalculatorImpl(parser.parseDateTime("1-Oct-2014"));

    // check U8
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("1-Oct-2006")), new Integer(8));
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("30-Sep-2007")), new Integer(8));
  }

  /**
   * Test the age group calculator.
   *
   * @throws Exception if something terrible happens.
   */
  @Test
  public void testU9ForOct2014() throws Exception {

    // setup a calculator for the 2014/2015 season
    final AgeGroupCalculatorImpl ageGroupCalculator = new AgeGroupCalculatorImpl(parser.parseDateTime("1-Oct-2014"));

    // check U9
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("1-Oct-2005")), new Integer(9));
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("30-Sep-2006")), new Integer(9));
  }

  /**
   * Test the age group calculator.
   *
   * @throws Exception if something terrible happens.
   */
  @Test
  public void testU10ForOct2014() throws Exception {

    // setup a calculator for the 2014/2015 season
    final AgeGroupCalculatorImpl ageGroupCalculator = new AgeGroupCalculatorImpl(parser.parseDateTime("1-Oct-2014"));

    // check U10
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("1-Oct-2004")), new Integer(10));
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("30-Sep-2005")), new Integer(10));
  }

  /**
   * Test the age group calculator.
   *
   * @throws Exception if something terrible happens.
   */
  @Test
  public void testU11ForOct2014() throws Exception {

    // setup a calculator for the 2014/2015 season
    final AgeGroupCalculatorImpl ageGroupCalculator = new AgeGroupCalculatorImpl(parser.parseDateTime("1-Oct-2014"));

    // check U11
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("1-Oct-2003")), new Integer(11));
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("30-Sep-2004")), new Integer(11));
  }

  /**
   * Test the age group calculator.
   *
   * @throws Exception if something terrible happens.
   */
  @Test
  public void testU12ForOct2014() throws Exception {

    // setup a calculator for the 2014/2015 season
    final AgeGroupCalculatorImpl ageGroupCalculator = new AgeGroupCalculatorImpl(parser.parseDateTime("1-Oct-2014"));

    // check U12
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("1-Oct-2002")), new Integer(12));
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("30-Sep-2003")), new Integer(12));
  }

  /**
   * Test the age group calculator.
   *
   * @throws Exception if something terrible happens.
   */
  @Test
  public void testU13ForOct2014() throws Exception {

    // setup a calculator for the 2014/2015 season
    final AgeGroupCalculatorImpl ageGroupCalculator = new AgeGroupCalculatorImpl(parser.parseDateTime("1-Oct-2014"));

    // check U13
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("1-Oct-2001")), new Integer(13));
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("30-Sep-2002")), new Integer(13));
  }

  /**
   * Test the age group calculator.
   *
   * @throws Exception if something terrible happens.
   */
  @Test
  public void testU14ForOct2014() throws Exception {

    // setup a calculator for the 2014/2015 season
    final AgeGroupCalculatorImpl ageGroupCalculator = new AgeGroupCalculatorImpl(parser.parseDateTime("1-Oct-2014"));

    // check U14
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("1-Oct-2000")), new Integer(14));
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("30-Sep-2001")), new Integer(14));
  }

  /**
   * Test the age group calculator.
   *
   * @throws Exception if something terrible happens.
   */
  @Test
  public void testU15ForOct2014() throws Exception {

    // setup a calculator for the 2014/2015 season
    final AgeGroupCalculatorImpl ageGroupCalculator = new AgeGroupCalculatorImpl(parser.parseDateTime("1-Oct-2014"));

    // check U15
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("1-Oct-1999")), new Integer(15));
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("30-Sep-2000")), new Integer(15));
  }

  /**
   * Test the age group calculator.
   *
   * @throws Exception if something terrible happens.
   */
  @Test
  public void testU16ForOct2014() throws Exception {

    // setup a calculator for the 2014/2015 season
    final AgeGroupCalculatorImpl ageGroupCalculator = new AgeGroupCalculatorImpl(parser.parseDateTime("1-Oct-2014"));

    // check U16
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("1-Oct-1998")), new Integer(16));
    Assert.assertEquals(ageGroupCalculator.getAgeGroup(parser.parseDateTime("30-Sep-1999")), new Integer(16));
  }

  /**
   * Test the age group calculator.
   *
   * @throws Exception if something terrible happens.
   */
  @Test
  public void testTooYoungForOct2014() throws Exception {

    // setup a calculator for the 2014/2015 season
    final AgeGroupCalculatorImpl ageGroupCalculator = new AgeGroupCalculatorImpl(parser.parseDateTime("1-Oct-2014"));

    // check kids that are too young
    Assert.assertNull(ageGroupCalculator.getAgeGroup(parser.parseDateTime("1-Oct-2009")));
  }

  /**
   * Test the age group calculator.
   *
   * @throws Exception if something terrible happens.
   */
  @Test
  public void testTooOldForOct2014() throws Exception {

    // setup a calculator for the 2014/2015 season
    final AgeGroupCalculatorImpl ageGroupCalculator = new AgeGroupCalculatorImpl(parser.parseDateTime("1-Oct-2014"));

    // check kids that are too old
    Assert.assertNull(ageGroupCalculator.getAgeGroup(parser.parseDateTime("30-Sep-1998")));
  }
  
} // age group calculator test